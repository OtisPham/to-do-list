import app from './app'
const port: any = process.env.PORT || 3000
import { sequelize } from './src/api/v1/configs/database';

app.listen(port, () => {
    console.log(`Server running on http://localhost:${port}`)
    sequelize.authenticate()
        .then(async () => {
            console.log('Connection has been established successfully.')
            try {
                await sequelize.sync({ force: false })
            } catch (error: any) {
                console.log(error.message)
            }
        }).catch(err => {
            console.error('Unable to connect to the database:', err)
        })
})