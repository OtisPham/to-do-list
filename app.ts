import express, { Express, NextFunction, Application, Request, Response, Router } from 'express'
require('dotenv').config()
import routerIndex from './src/api/v1/router'


class App {
   public app: Application;

   constructor() {
      this.app = express();
      this.config();
   }
   private config(): void {

      this.app.use((req: Request, res: Response, next: NextFunction) => {
         res.header('Access-Control-Allow-Origin', '*');
         res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
         res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
         next();
      })


      this.app.use(express.json());
      this.app.use(express.urlencoded({ extended: false }));

      this.app.use('/api/v1', routerIndex)

      this.app.use('/', (req: Request, res: Response, next: NextFunction) => {
         res.send('Server running on http://localhost:3000')
      })

      this.app.use(((err: Error, req: Request, res: Response, next: NextFunction) => {
         res.send({ message: err.message })
      }));
   }

}


export default new App().app;