import { Request, Response, NextFunction } from "express"
import jwt from "jsonwebtoken"
import AccountService from "../accounts/account.service"
import UserService from "../users/user.service"


class Passport {
    constructor() { }

    jwtAuthentication = (req: Request, res: Response, next: NextFunction): void => {
        this.verifyToken(req, res, next)
    }

    private async verifyToken(req: Request, res: Response, next: NextFunction): Promise<void> {
        const bearerHeader = req.headers['authorization']
        if (typeof bearerHeader !== 'undefined') {
            const bearer = bearerHeader.split(' ')
            const bearerToken = bearer[1]
            try {
                var decode: any = jwt.verify(bearerToken, process.env.JWT_SECRET || 'secret');
                if (decode) {
                    const account: any = await AccountService.findOne({ where: { id: decode.id }, raw: true, attributes: ['id', 'username', 'role'] })
                    const user: any = await UserService.findOne({ where: { accountId: account.id }, raw: true, attributes: ['id', 'firstName', 'lastName'] })
                    req.account = account
                    req.user = user
                    next()

                }
            } catch (err: any) {
                // err
                res.status(401)
                next(err)
            }
        } else {
            res.sendStatus(401)
        }
    }

}


export default new Passport()