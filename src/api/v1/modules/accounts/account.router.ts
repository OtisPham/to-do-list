import { Router } from "express";

import AccountController from "./account.controller";
import passportMiddleware from "../middleware/passport.middleware";


class AuthRouter {
    routers: Router;

    constructor() {
        this.routers = Router()
        this.config()
    }

    config() {
        this.routers.post("/", AccountController.createAccount);
        this.routers.post("/login", AccountController.login);
        this.routers.get("/", AccountController.getAllAccounts);
        this.routers.get("/:id", AccountController.getAccountById);
        this.routers.put("/:id", AccountController.updateAccount);
    }
}

export default new AuthRouter().routers;