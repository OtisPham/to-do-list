import Account, { IAccount, ERole } from '../../configs/database/models/account.model';
import jwt from 'jsonwebtoken';

export default class AccountService extends Account {

    public static async createAccount(username: string, password: string, role: ERole): Promise<any> {
        try {
            const account: IAccount = {
                username, password, role
            }
            return await Account.create({ ...account });
        } catch (error: any) {
            return { error, code: 400 };
        }
    }

    public static async getAllAccounts(): Promise<any> {
        const accounts = await Account.findAll({ attributes: ['id', 'username', 'role'] });
        return { accounts };
    }

    public static async getAccountByUsername(username: string): Promise<any> {
        const account = await Account.findOne({ where: { username }, attributes: ['username', 'id', 'role'] });
        if (!account) {
            return { error: new Error('Username not found'), code: 404 };
        }
        return { account };
    }

    public static async getAccountByUsernameAndPassword(username: string, password: string): Promise<any> {
        const account = await Account.findOne({ where: { username }, attributes: ['id', 'username', 'password', 'role'] });
        if (!account) {
            return { error: new Error('Invalide username'), code: 400 };
        }
        const isValid = await account.comparePassword(password);
        if (!isValid) {
            return { error: new Error('Invalid username or password'), code: 400 };
        }
        const JWT_SECRET: string = process.env.JWT_SECRET || 'secret';
        const accesstToken = jwt.sign({ id: account.id }, JWT_SECRET, { expiresIn: '1d' });
        const JWT_REFRESH_SECRET: string = process.env.JWT_REFRESH_SECRET || 'jwt_refresh_secret';
        const refreshToken = jwt.sign({ id: account.id }, JWT_REFRESH_SECRET, { expiresIn: '7d' });
        account.refreshToken = refreshToken
        account.save();
        const { id, role } = account;
        return { accesstToken, id, username, role, refreshToken }
    }

    public static async getAccountById(id: number): Promise<any> {
        const account = await Account.findOne({ where: { id }, attributes: ['id', 'username', 'role'] });
        if (!account) {
            return { error: new Error('Account not found'), code: 404 };
        }
        return { account };
    }

    public static async updateAccount(id: number, body: any): Promise<any> {
        try {
            const account = await Account.update(body, { where: { id } });
            return { account }
        } catch (error) {
            return { error, code: 500 };
        }
    }
}


