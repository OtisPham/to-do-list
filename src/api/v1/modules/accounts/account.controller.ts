import { NextFunction, Request, Response } from "express";
import AccountService from "./account.service";
import UserService from "../users/user.service";

export default class AuthController {
    public static async createAccount(req: Request, res: Response, next: NextFunction) {
        try {
            const { username, password, role, firstName, lastName } = req.body;
            const account = await AccountService.createAccount(username, password, role);
            if (account.error) {
                res.status(account.code)
                next(account.error);
                return
            }
            const user = await UserService.createUser(firstName, lastName, account.id);
            res.status(201).json({
                message: "ok",
                account: JSON.parse(JSON.stringify(account)),
                user: JSON.parse(JSON.stringify(user)),
            })
        } catch (error) {
            res.status(500)
            next(error);
        }
    }

    public static async login(req: Request, res: Response, next: NextFunction) {
        const { username, password } = req.body;
        const { error, code, accesstToken, ...account } = await AccountService.getAccountByUsernameAndPassword(username, password);
        if (error) {
            res.status(code)
            next(error);
            return
        }
        const user = await UserService.getUserByAccountId(account.id);
        res.status(200).json({
            message: "ok",
            user,
            account,
            accesstToken
        })
    }

    public static async getAllAccounts(req: Request, res: Response, next: NextFunction) {
        try {
            const accounts = await AccountService.getAllAccounts();
            res.status(200).json({
                message: "ok",
                accounts
            })
        } catch (error) {
            res.status(500)
            next(error);
        }
    }

    public static async getAccountById(req: Request, res: Response, next: NextFunction) {
        try {
            const id: any = parseInt(req.params.id);
            const { error, code, account } = await AccountService.getAccountById(id);
            if (error) {
                res.status(code)
                next(error);
                return
            }
            res.status(200).json({
                message: "ok",
                account
            })
        } catch (error) {
            res.status(500)
            next(error);
        }
    }

    public static async updateAccount(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.params;
            if (id == req.account.id) {
                const { password } = req.body
                const { error, code, account } = await AccountService.updateAccount(parseInt(id), { password });
                if (error) {
                    res.status(code)
                    next(error);
                    return
                }
                res.status(200).json({
                    message: "ok",
                    account
                })
            } else {
                res.status(403)
                next(new Error("You are not allowed to update this account"));
            }
        } catch (error) {
            res.status(500)
            next(error);
        }
    }

}