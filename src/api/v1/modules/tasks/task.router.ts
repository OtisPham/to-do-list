import { Router } from "express";
import TaskController from "./task.controller";
import passportMiddleware from "../middleware/passport.middleware";

class TaskRouter {
    routers: Router;
    constructor() {
        this.routers = Router();
        this.config();
    }
    config() {
        this.routers.post('/', passportMiddleware.jwtAuthentication, TaskController.createTask);
        this.routers.put('/:id', passportMiddleware.jwtAuthentication, TaskController.updateTask);
        this.routers.delete('/:id', passportMiddleware.jwtAuthentication, TaskController.deleteTask);
    }
}


export default new TaskRouter().routers;