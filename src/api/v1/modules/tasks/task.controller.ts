import { NextFunction, Request, Response } from "express";
import TaskService from "./task.service";


export default class TaskController {

    public static async createTask(req: Request, res: Response, next: NextFunction) {
        try {
            const { title, description, status, listId } = req.body;
            const task = await TaskService.createTask(title, description, status, listId);
            res.status(201).json({
                message: "ok",
                task
            })
        } catch (error) {
            res.status(500)
            next(error);
        }
    }
    public static async updateTask(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.params;
            await TaskService.updateTask(parseInt(id), req.body);
            res.status(200).json({
                message: "ok"
            })
        } catch (error) {
            res.status(500)
            next(error);
        }
    }
    public static async deleteTask(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.params;
            const task = await TaskService.deleteTask(parseInt(id));
            res.status(200).json({
                message: "ok",
                task
            })
        } catch (error) {
            res.status(500)
            next(error);
        }
    }
}
