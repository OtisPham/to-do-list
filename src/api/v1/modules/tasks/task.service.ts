import Task, { ITask } from '../../configs/database/models/task.model';



export default class TaskService extends Task {

    public static async createTask(title: string, description: string, status: boolean, listId: number): Promise<any> {
        try {
            const task: ITask = { title, description, status, listId }
            return await Task.create({ ...task });
        } catch (error: any) {
            return { error, code: 500 };
        }
    }

    public static async getTasksByListId(listId: number): Promise<any> {
        try {
            const task = await Task.findAll({
                where: { listId }, raw: true, attributes: ['id', 'title', 'description', 'status', 'listId']
            });
            return task
        } catch (error: any) {
            return { error: error, code: 500 };
        }
    }

    public static async getTaskById(id: number): Promise<any> {
        try {
            const task = await Task.findOne({
                where: { id }, raw: true
            });
            if (!task) {
                return { error: new Error('Task not found'), code: 404 };
            }
            return task
        } catch (error: any) {
            return { error, code: 500 };
        }
    }

    public static async updateTask(id: number, body: any): Promise<any> {
        try {
            const task = await Task.update(body, { where: { id } });
            return task
        } catch (error: any) {
            return { error, code: 500 };
        }
    }

    public static async deleteTask(id: number): Promise<any> {
        try {
            const task = await Task.destroy({ where: { id } });
            return task
        } catch (error: any) {
            return { error, code: 500 };
        }
    }
}