import { NextFunction, Request, Response } from "express";
import UserService from "./user.service";

export default class AuthController {
    public static async createUser(req: Request, res: Response, next: NextFunction) {
        try {
            const { firstName, lastName } = req.body;
            const accountId = req.accountId
            const user = await UserService.createUser(firstName, lastName, accountId);
            res.status(201).json({
                message: "ok",
                user
            })
        } catch (error) {
            res.status(500)
            next(error);
        }
    }

    public static async getAllUsers(req: Request, res: Response, next: NextFunction) {
        try {
            const users = await UserService.getAllUsers();
            res.status(200).json({
                message: "ok",
                users
            })
        } catch (error) {
            res.status(500)
            next(error);
        }
    }

    public static async getUserByAccountId(req: Request, res: Response, next: NextFunction) {
        try {
            const user = await UserService.getUserByAccountId(req.account.id);
            res.status(200).json({
                message: "ok",
                user
            })
        } catch (error) {
            res.status(500)
            next(error);
        }
    }

    public static async getUserById(req: Request, res: Response, next: NextFunction) {
        try {
            const id: any = parseInt(req.params.id);
            const { error, code, user } = await UserService.getUserById(id);
            if (error) {
                res.status(code)
                next(error);
                return
            }
            res.status(200).json({
                message: "ok",
                user
            })
        } catch (error) {
            res.status(500)
            next(error);
        }
    }

    public static async updateUser(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.params;
            if (id == req.user.id) {
                const user = await UserService.updateUser(parseInt(id), req.body);
                res.status(200).json({
                    message: "ok",
                    user
                })
            } else {
                next(new Error("You can't update other user"))
            }
        } catch (error) {
            res.status(500)
            next(error);
        }
    }
}