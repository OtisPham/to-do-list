
import User, { IUser } from "../../configs/database/models/user.model";


export default class UserService extends User {
    public static async createUser(firstName: string, lastName: string, accountId: any) {
        try {
            const user: IUser = { firstName, lastName, accountId }
            return await User.create({ ...user });
        } catch (error: any) {
            return { error, code: 500 };
        }
    }

    public static async getAllUsers(): Promise<any> {
        try {
            const users = await User.findAll({ raw: true });
            return users
        } catch (error: any) {
            return { error, code: 500 };
        }
    }

    public static async getUserByAccountId(accountId: number): Promise<any> {
        try {
            const user = await User.findOne({
                where: { accountId }, raw: true, attributes: ['id', 'accountId', 'firstName', 'lastName']
            });
            return user
        } catch (error: any) {
            return { error: error, code: 500 };
        }
    }

    public static async getUserById(id: number): Promise<any> {
        try {
            const user = await User.findOne({
                where: { id }
            });
            if (!user) {
                return { error: new Error('User not found'), code: 404 };
            }
            return { user }
        } catch (error: any) {
            return { error, code: 500 };
        }
    }

    public static async updateUser(id: number, body: any): Promise<any> {
        try {
            const user = await User.update(body, { where: { id } });
            return user
        } catch (error: any) {
            return { error, code: 500 };
        }
    }


}