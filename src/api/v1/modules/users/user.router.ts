import { Router } from "express";

import UserController from "../users/user.controller";
import passportMiddleware from "../middleware/passport.middleware";

class UserRouter {
    routers: Router;

    constructor() {
        this.routers = Router()
        this.config()
    }

    config() {
        // this.routers.get("/", passportMiddleware.jwtAuthentication, UserController.getUserByAccountId);
        this.routers.get("/", UserController.getAllUsers);
        this.routers.get("/:id", UserController.getUserById);
        this.routers.put("/:id", passportMiddleware.jwtAuthentication, UserController.updateUser);
    }
}

export default new UserRouter().routers;