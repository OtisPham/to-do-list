import { NextFunction, Request, Response } from "express";
import ListService from "./list.service";


export default class ListTaskController {
    public static async getListTasks(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.user
            const list = await ListService.getListsByUserId(id);
            res.status(200).json({
                message: "ok",
                list
            })
        } catch (error) {
            res.status(500)
            next(error);
        }
    }

    public static async createList(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.user
            const { name } = req.body;
            const list = await ListService.createList(name, id);
            res.status(201).json({
                message: "ok",
                list
            })
        } catch (error) {
            res.status(500)
            next(error);
        }
    }

    public static async updateList(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.params
            const { name } = req.body;
            const list = await ListService.getListById(parseInt(id));
            if (list.id === req.user.id) {
                await ListService.updateList(name, parseInt(id));
                res.status(200).json({
                    message: "ok"
                })
            } else {
                res.status(403)
                next(new Error("You are not allowed to update this list"))
            }
        } catch (error) {
            res.status(500)
            next(error);
        }
    }

    public static async deleteList(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.params;
            const list = await ListService.getListById(parseInt(id));
            if (list.id === req.user.id) {
                await ListService.deleteList(parseInt(id));
                res.status(200).json({
                    message: "ok",
                })
            } else {
                res.status(403)
                next(new Error("You are not allowed to delete this list"))
            }
        } catch (error) {
            res.status(500)
            next(error);
        }
    }

}
