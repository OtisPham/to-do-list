import { Router } from "express";
import ListController from "./list.controller";
import passportMiddleware from "../middleware/passport.middleware";

class ListRouter {
    routers: Router;
    constructor() {
        this.routers = Router();
        this.config();
    }
    config() {
        this.routers.get('/', passportMiddleware.jwtAuthentication, ListController.getListTasks);
        this.routers.post('/', passportMiddleware.jwtAuthentication, ListController.createList);
        this.routers.put('/:id', passportMiddleware.jwtAuthentication, ListController.updateList);
        this.routers.delete('/:id', passportMiddleware.jwtAuthentication, ListController.deleteList);
    }
}


export default new ListRouter().routers;