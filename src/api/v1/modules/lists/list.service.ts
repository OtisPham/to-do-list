import List, { IList } from "../../configs/database/models/list.model";
import Task from '../../configs/database/models/task.model';

export default class ListService extends List {
    public static async createList(name: string, userId: number) {
        try {
            const list: IList = { name, userId }
            return await List.create({ ...list });
        } catch (error: any) {
            return { error, code: 500 };
        }
    }
    public static async getListsByUserId(userId: number): Promise<any> {
        try {
            const list = await List.findAll({
                include: [
                    {
                        model: Task,
                        required: false
                    }
                ],
                group: ["List.id", "tasks.id"],
                where: { userId }, raw: true, attributes: ['id', 'name', 'userId'],
            });
            return list
        } catch (error: any) {
            console.log(error);
            return { error: error, code: 500 };
        }
    }
    public static async getListById(id: number): Promise<any> {
        try {
            const list = await List.findOne({
                where: { id }, raw: true
            });
            if (!list) {
                return { error: new Error('List not found'), code: 404 };
            }
            return list
        } catch (error: any) {
            return { error, code: 500 };
        }
    }

    public static async updateList(name: string, id: number): Promise<any> {
        try {
            const list = await List.update({ name }, { where: { id } });
            return list
        } catch (error: any) {
            return { error, code: 500 };
        }
    }

    public static async deleteList(id: number): Promise<any> {
        try {
            const list = await List.destroy({ where: { id } });
            return list
        } catch (error: any) {
            return { error, code: 500 };
        }
    }
}