import { Router } from 'express'
import userRouters from "./modules/users/user.router";
import accountRouters from "./modules/accounts/account.router";
import taskRouter from "./modules/tasks/task.router";
import listRouter from "./modules/lists/list.router";

class RouterIndex {

    public routers: Router;

    constructor() {
        this.routers = Router()
        this.config()
    }

    config() {
        this.routers.use('/list', listRouter)
        this.routers.use('/task', taskRouter)
        this.routers.use('/user', userRouters)
        this.routers.use('/account', accountRouters)
    }
}

export default new RouterIndex().routers;