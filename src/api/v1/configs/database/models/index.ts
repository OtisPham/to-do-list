import Account from './account.model';
import User from './user.model';
import List from './list.model';
import Task from './task.model';

export const models = [Account, User, List, Task];

