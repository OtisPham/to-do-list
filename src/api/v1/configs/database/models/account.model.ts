import { Table, Index, Column, Model, DataType, HasMany, BeforeCreate, HasOne, PrimaryKey } from 'sequelize-typescript'
import bcrypt from 'bcryptjs';
import User from './user.model';

export enum ERole {
    Admin = 'Admin',
    User = 'User'
}

export interface IAccount {
    id?: number;
    username: string;
    password: string;
    refreshToken?: string | null;
    role: ERole;
}

@Table({
    timestamps: true,
    tableName: 'accounts'
})
export default class Account extends Model implements IAccount {
    @PrimaryKey
    @Column({ autoIncrement: true, type: DataType.INTEGER })
    id?: number;

    @Index
    @Column({
        unique: { name: 'username', msg: 'Username already exists' },
        type: DataType.STRING, allowNull: false,
        validate: {
            notNull: {
                msg: 'Please enter your username'
            }
        }
    })
    username!: string;

    @Column({ type: DataType.STRING, allowNull: false })
    password!: string;

    @Column({ type: DataType.TEXT, allowNull: true })
    refreshToken?: string | null;

    @Column({ type: DataType.STRING, allowNull: false })
    role!: ERole;

    @HasOne(() => User)
    user?: User;

    async comparePassword(password: string): Promise<boolean> {
        return await bcrypt.compare(password, this.password);
    }

    @BeforeCreate
    static async hashPassword(instance: Account) {
        instance.password = await bcrypt.hash(instance.password, 10);
    }
}
