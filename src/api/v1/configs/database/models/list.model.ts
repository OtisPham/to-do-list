import { Table, Index, ForeignKey, Column, Model, HasMany, BeforeCreate, BelongsTo, PrimaryKey, DataType } from 'sequelize-typescript'
import User from './user.model';
import Task from './task.model';


export interface IList {
    id?: number;
    name: string;
    userId: number;
}

@Table({
    timestamps: true,
    tableName: 'list'
})
export default class List extends Model implements IList {
    @PrimaryKey
    @Column({
        type: DataType.INTEGER,
        autoIncrement: true,
        allowNull: false
    })
    id?: number;

    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    name!: string;

    @ForeignKey(() => User)
    @Column({
        type: DataType.INTEGER,
        allowNull: false
    })
    userId!: number;

    @HasMany(() => Task)
    tasks?: Task[];
}
