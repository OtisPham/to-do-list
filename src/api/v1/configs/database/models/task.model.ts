import { Table, Index, ForeignKey, Column, Model, HasMany, BeforeCreate, BelongsTo, PrimaryKey, DataType } from 'sequelize-typescript'
import List from './list.model';

export interface ITask {
    id?: number;
    listId: number;
    title: string;
    description: string;
    status: boolean;
    createdAt?: Date;
    updatedAt?: Date;
}

@Table({
    timestamps: true,
    tableName: 'task'
})
export default class Task extends Model implements ITask {
    @PrimaryKey
    @Column({
        type: DataType.INTEGER,
        autoIncrement: true,
        allowNull: false
    })
    id?: number;

    @ForeignKey(() => List)
    @Column({
        type: DataType.INTEGER,
        allowNull: false
    })
    listId!: number;

    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    title!: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        defaultValue: ''
    })
    description!: string;

    @Column({
        type: DataType.BOOLEAN,
        allowNull: false,
        defaultValue: false
    })
    status!: boolean;

    @BelongsTo(() => List)
    list!: List;
}
