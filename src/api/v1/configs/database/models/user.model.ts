import { Table, Index, ForeignKey, Column, Model, HasMany, BeforeCreate, BelongsTo, PrimaryKey, DataType } from 'sequelize-typescript'
import Account from './account.model';

export enum EGender {
    Nam = 'Nam',
    Nu = 'Nữ'
}

export interface IUser {
    id?: number;
    accountId: number;
    firstName: string;
    lastName: string;
    gender?: EGender;
    phone?: string;
    dayOfBirth?: string;
}

@Table({
    timestamps: true,
    tableName: 'users'
})
export default class User extends Model implements IUser {

    @PrimaryKey
    @Column({ autoIncrement: true, type: DataType.INTEGER })
    id?: number;

    @ForeignKey(() => Account)
    @Column({ allowNull: false, type: DataType.INTEGER })
    accountId!: number;

    @BelongsTo(() => Account)
    account?: Account;

    @Column({ type: DataType.STRING, allowNull: false })
    firstName!: string;

    @Column({ type: DataType.STRING, allowNull: false })
    lastName!: string;

    @Column({ type: DataType.STRING, allowNull: true })
    gender?: EGender;

    @Column({ type: DataType.STRING, allowNull: true })
    phone?: string;

    @Column({ type: DataType.STRING, allowNull: true })
    dayOfBirth?: string;

}
