import { Sequelize } from 'sequelize-typescript'
import { models } from './models'

const DATABASE_NAME: string = process.env.DATABASE_NAME || 'database';
const DATABASE_USER: string = process.env.DATABASE_USER || 'postgres';
const DATABASE_PASSWORD: string = process.env.DATABASE_PASSWORD || 'admin';

export const sequelize = new Sequelize(DATABASE_NAME, DATABASE_USER, DATABASE_PASSWORD,
  { host: 'localhost', dialect: 'postgres', models: models }
)
